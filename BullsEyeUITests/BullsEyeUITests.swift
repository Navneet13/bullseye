import XCTest

class BullsEyeUITests: XCTestCase {

    override func setUp() {
      
    }

    override func tearDown() {
      
    }
  
  //Recalling Selenium Testing
  //1. Find the Element
  //2. Do something with the element  i) Like -> Click on element ii) Send the keys
  
  //iOS unit testing follows same process as selenium

  //T1: When the person presses the Hit Me button it should show a pop up menu
    func testPressingHitShowsALertBox() {
       //0. Start the app
       let app = XCUIApplication()
      
      //1. Find the Hit Me button
        let hitMeButton = app.buttons["hit me!"]
      
       //2. Cheeck Hit Me button exists
      XCTAssertEqual(true, hitMeButton.exists)
      
      //3. Click on the Hit Me button
      hitMeButton.tap()
      
      //4.  Check that alert appears(pop up)
    
      //4a. Get the Alert Box
      let alertBox = app.alerts
      
      //4b. Check the Alert Box exists
      XCTAssertNotNil(alertBox)
  
    }

  //T2: BY default, label at top of the screen says "Get as close as you can to: "
  func testDefaultLabelCode(){
    
    //0. Start the app
    let app = XCUIApplication()
    
    //1. Find the label at the top
    let label = app.staticTexts["Get as close as you can to: "]
    
    //2. Check that it exists
    XCTAssertNotNil(label)
    
    //3. Check that the text in the label matches u expect "Get as close as you can to: "
   XCTAssertEqual("Get as close as you can to: ",label.label)
  }
  

//  //T3: Pressing the type button update the label at the top of the screen to say:
//  func testStartOverButton(){
//    app.buttons["start over"].tap()
//
//  }

  
  //T4: Pressing the Start Over button resets the score and round label text
  func testTypeGameMode(){
    let app = XCUIApplication()
    // 1. get the type button
    let typeButton = app/*@START_MENU_TOKEN@*/.buttons["Type"]/*[[".segmentedControls.buttons[\"Type\"]",".buttons[\"Type\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
    // 2. test that button exists
    XCTAssertEqual(true, typeButton.exists)
    // 3. press the button
    typeButton.tap();
    // 4. get the label at top of scrren
    let label = app.staticTexts["Guess where the slider is: "]
    // 5. check that label exists
    XCTAssertNotNil(label)
    // 6. check the text in the label
    XCTAssertEqual("Guess where the slider is: ", label.label)
  }
  
}
